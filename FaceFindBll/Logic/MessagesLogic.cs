﻿using Common;
using FaceFind.Dal;
using FaceFind.Entities.BO;
using FaceFindBll.Interfaces;
using System;
using System.Collections.Generic;

namespace FaceFindBll.Logic
{
    class MessagesLogic : IMessagesLogic
    {
        IMessageRepository repository;

        public MessagesLogic()
        {
            repository = RepositoryProvider.GetRepository<IMessageRepository>();
        }

        public void SendMessage(Message msg, Guid dialogId)
        {
            repository.AddMessage(msg.ToDTOObject(), dialogId);
        }

        public Dialog GetUserDialog(string userLogin, Guid dialogId)
        {
            return new Dialog(repository.GetDialog(dialogId));
        }

        public IEnumerable<Dialog> GetUserDialogs(string userLogin)
        {
            var res = new List<Dialog>();
            repository.GetUserDialogs(userLogin).ForEach(d =>
            {
                res.Add(new Dialog(d));
            });
            return res;
        }
    }
}
