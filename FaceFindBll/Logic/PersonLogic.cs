﻿using FaceFind.Dal;
using FaceFind.Entities.BO;
using FaceFindBll.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FaceFindBll.Logic
{
    class PersonLogic : IPersonLogic
    {
        readonly IPersonRepository repository;

        public PersonLogic()
        {
            repository = RepositoryProvider.GetRepository<IPersonRepository>();
        }
        /// <summary>
        /// Изменяет личную информацию о пользователе (только свойства примитивных типов)
        /// </summary>
        /// <param name="person"></param>
        public void UpdatePerson(Person person)
        {
            repository.UpdatePerson(person.ToDTOObject());
        }

        /// <summary>
        /// Получает личную информацию пользователя по его основному id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Person GetPersonById(Guid id) => new Person(repository.GetPersonById(id));

        /// <summary>
        /// Получает личную информацию пользователя по его логину
        /// </summary>
        /// <param name="userLogin"></param>
        /// <returns></returns>
        public Person GetPersonByUserLogin(string userLogin) => new Person(repository.GetPersonByUserLogin(userLogin));

        public IEnumerable<Person> GetAllPeople() => repository.GetAllPeople().Select(p => new Person(p));

        public IEnumerable<Person> GetUserFollowers(Guid personId) => repository.GetFollowers(personId).Select(p => new Person(p));

        public IEnumerable<Person> GetFollowedPersons(Guid personId) => repository.GetFollowed(personId).Select(p => new Person(p));

        public void FollowPerson(Guid personId, Guid followerId)
        {
            repository.FollowPerson(personId, followerId);
        }

        public void UnfollowPerson(Guid personId, Guid followerId)
        {
            repository.UnfollowPerson(personId, followerId);
        }
    }
}
