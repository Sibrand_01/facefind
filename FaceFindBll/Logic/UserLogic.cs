﻿using FaceFind.Bll;
using FaceFind.Dal;
using FaceFind.Entities.BO;
using FaceFindBll.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FaceFindBll.Logic
{
    public class UserLogic : IUserLogic
    {
        private IUserRepository repository;

        public UserLogic()
        {
            repository = RepositoryProvider.GetRepository<IUserRepository>();
        }
        /// <summary>
        /// Добавляет нового пользователя в базу и добавляет ему роль простого пользователя, если у него такой не было.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="name">todo: describe name parameter on AddNewUser</param>
        /// <param name="surname">todo: describe surname parameter on AddNewUser</param>
        public void AddNewUser(User user, string name, string surname)
        {
            repository.AddNewUser(user.ToDTOObject(), name, surname);
        }

        /// <summary>
        /// Добавляет пользователю новую роль
        /// </summary>
        /// <param name="login"></param>
        /// <param name="role"></param>
        public void AddUserRole(string login, string role)
        {
            repository.AddRoleToUser(login, role);
        }

        public void BanPerson(Guid personId)
        {
            repository.AddRoleToUserById(personId, Role.BannedRoleTitle);
        }

        /// <summary>
        /// Проверяет, что представленная комбинация логина и пароля верна
        /// </summary>
        /// <param name="userVm"></param>
        /// <returns></returns>
        public bool CanAuthenticate(string login, string password)
        {
            var checkUser = new User(repository.GetUserByLogin(login));
            if (checkUser == null)
            {
                return false;
            }
            return checkUser.PasswordHash == HashGenerator.GetHash(password);
        }
        /// <summary>
        /// Меняет пароль пользоватля
        /// </summary>
        /// <param name="login"></param>
        /// <param name="newPassword"></param>
        public void ChangePassword(string login, string newPassword)
        {
            repository.ChangeUserPassword(login, newPassword);
        }

        /// <summary>
        /// Удаляет роль пользователя
        /// </summary>
        /// <param name="login"></param>
        /// <param name="role"></param>
        public void DeleteUserRole(string login, string role)
        {
            repository.DeleteUserFromRole(login, role);
        }


        /// <summary>
        /// Получает роли для данного пользователя
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public IEnumerable<Role> GetRolesForUser(string login) => repository.GetRolesForUser(login).Select(r => new Role(r));

        /// <summary>
        /// Получает пользователя по его id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public User GetUserById(Guid id) => new User(repository.GetUserById(id));

        /// <summary>
        /// Получает пользователя по его логину
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public User GetUserByLogin(string login) => new User(repository.GetUserByLogin(login));

        public IEnumerable<Guid> GetUsersIdByRole(string role) => repository.GetUserIdsByRole(role);

        /// <summary>
        /// Проверяет, есть такой логин в базе
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public bool IsLoginExists(string login) => repository.IsLoginExists(login);

        /// <summary>
        /// Проверяет, состоит ли пользователь в данной роли
        /// </summary>
        /// <param name="login"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        public bool IsUserInRole(string login, string role) => repository.IsUserInRole(login, role);

        public void UnbanPerson(Guid personId)
        {
            repository.DeleteUserFromRoleById(personId, Role.BannedRoleTitle);
        }

    }
}
