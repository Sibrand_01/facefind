﻿using FaceFind.Dal;
using FaceFind.Entities.BO;
using FaceFindBll.Interfaces;
using System;

namespace FaceFindBll.Logic
{
    class PhotoLogic : IPhotoLogic
    {
        private IPhotoRepository repository;

        public PhotoLogic()
        {
            repository = RepositoryProvider.GetRepository<IPhotoRepository>();
        }
        /// <summary>
        /// Добавляет или заменяет фото пользователя
        /// </summary>
        /// <param name="photo"></param>
        /// <param name="userLogin"></param>
        public void AddOrChangePhoto(Photo photo)
        {
            repository.UploadOrUpdate(photo.ToDTOObject());
        }

        /// <summary>
        /// Удаляет фото текущего пользователя
        /// </summary>
        /// <param name="userLogin"></param>
        public void DeletePhoto(Guid id)
        {
            repository.RemovePhoto(id);
        }

        public Photo GetUserPhoto(Guid userId) => new Photo(repository.GetPersonPhoto(userId));
    }
}
