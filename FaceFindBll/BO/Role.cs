﻿using System;

namespace FaceFind.Entities.BO
{
    public class Role
    {
        public const string UserRoleTitle = "User";
        public const string AdminRoleTitle = "Admin";
        public const string SuperAdminRoleTitle = "SuperAdmin";
        public const string BannedRoleTitle = "Banned";

        public Role()
        {

        }
        public Role(DTO.Role dto)
        {
            Id = dto.Id;
            Title = dto.Title;
        }

        public Guid Id { get; private set; }
        public string Title { get;  private set; }

        public DTO.Role ToDTOObject() => new DTO.Role
        {
            Id = Id,
            Title = Title,
        };
}
}
