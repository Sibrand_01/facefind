﻿using System;

namespace FaceFind.Entities.BO
{
    /// <summary>
    /// Класс описывает сообщение.
    /// </summary>
    public class Message
    {
        public Message()
        {

        }
        public Message(DTO.Message msg)
        {
            Id = msg.Id;
            Sender = new Person
            {
                Id = msg.SenderId
            };
            SendDate = msg.SendDate;
            IsSeen = msg.IsSeen;
            Content = msg.Content;
        }
        public Guid Id { get; set; }
        public Person Sender { get; set; }
        public DateTime SendDate { get; set; }
        public bool IsSeen { get; set; }
        public string Content { get; set; }

        public DTO.Message ToDTOObject() => new DTO.Message
        {
            Id = Id,
            Content = Content,
            IsSeen = IsSeen,
            SendDate = SendDate,
            SenderId = Sender.Id
        };
    }
}