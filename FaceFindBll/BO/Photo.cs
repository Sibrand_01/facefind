﻿using System;
namespace FaceFind.Entities.BO
{
    /// <summary>
    /// Kласс, описывающий фотографию
    /// </summary>
    public class Photo
    {
        public Photo()
        {
        }
        public Photo(DTO.Photo dto)
        {
            Id = dto.Id;
            Content = dto.Content;
            ContentType = dto.ContentType;
        }
        public Guid Id { get; set; }
        public byte[] Content { get; set; }
        public string ContentType { get; set; }

        public DTO.Photo ToDTOObject() => new DTO.Photo
        {
            Id = Id,
            Content = Content,
            ContentType = ContentType
        };
    }
}
