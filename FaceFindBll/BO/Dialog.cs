﻿using System;
using System.Collections.Generic;

namespace FaceFind.Entities.BO
{
    /// <summary>
    /// Класс описывает диалог пользователей.
    /// </summary>
    public class Dialog
    {

        public Dialog(DTO.Dialog dialog)
        {
            Id = dialog.Id;
            Participant1 = new Person
            {
                Id = dialog.Participant1Id
            };
            Participant2 = new Person
            {
                Id = dialog.Participant2Id
            };
            Messages = new List<Message>();
        }
        public Dialog()
        {

        }
        public Guid Id { get; set; }
        public Person Participant1 { get; set; }
        public Person Participant2 { get; set; }
        public List<Message> Messages { get; set; }

        public DTO.Dialog ToDTOObject() => new DTO.Dialog
        {
            Id = Id,
            Participant1Id = Participant1.Id,
            Participant2Id = Participant2.Id
        };
    }
}