﻿using System;

namespace FaceFind.Entities.BO
{
    /// <summary>
    /// Модель пользователя, содержащая основную информацию для авторизации
    /// </summary>
    public class User
    {
        public User()
        {

        }
        public User(DTO.User dto)
        {
            Id = dto.Id;
            Login = dto.Login;
            RegistrationDate = dto.RegistrationDate;
            PasswordHash = dto.PasswordHash;
            PasswordChangeDate = dto.PasswordChangeDate;
        }
        public Guid Id { get; private set; }
        public string Login { get; private set; }
        public DateTime? RegistrationDate { get; private set; }
        public string PasswordHash { get; set; }
        public DateTime? PasswordChangeDate { get; private set; }

        public DTO.User ToDTOObject() => new DTO.User
        {
            Id = Id,
            Login = Login,
            RegistrationDate = RegistrationDate,
            PasswordHash = PasswordHash,
            PasswordChangeDate = PasswordChangeDate
        };

    }
}
