﻿using System;
using System.Collections.Generic;

namespace FaceFind.Entities.BO
{
    /// <summary>
    /// Основная информация о человеке
    /// </summary>
    public class Person
    {
        public Person(DTO.Person dto)
        {
            Id = dto.Id;
            Name = dto.Name;
            SecondName = dto.SecondName;
            Surname = dto.Surname;
            BirthDay = dto.BirthDay;
            Status = dto.Status;

            Followed = new List<Person>();
            Followers = new List<Person>();
        }
        public Person()
        {
            Followed = new List<Person>();
            Followers = new List<Person>();
        }
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string SecondName { get; set; }
        public DateTime? BirthDay { get; set; }
        public string Status { get; set; }
        public Photo Photo { get; set; }
        public List<Person> Followers { get; set; }
        public List<Person> Followed { get; set; }

        public DTO.Person ToDTOObject() => new DTO.Person
        {
            Id = Id,
            Name = Name,
            Surname = Surname,
            BirthDay = BirthDay,
            Status = Status,
            SecondName = SecondName
        };
    }
}