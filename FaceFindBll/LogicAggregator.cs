﻿
using FaceFindBll.Interfaces;
using FaceFindBll.Logic;

namespace FaceFindBll
{
    public static class LogicAggregator
    {
        static IMessagesLogic _mLogic;
        static IUserLogic _uLogic;
        static IPhotoLogic _phLogic;
        static IPersonLogic _pLogic;

        static LogicAggregator()
        {
            _mLogic = new MessagesLogic();
            _uLogic = new UserLogic();
            _phLogic = new PhotoLogic();
            _pLogic = new PersonLogic();
        }


    }
}
