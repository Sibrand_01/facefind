﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace FaceFind.Bll
{

    public class HashGenerator
    {
        /// <summary>
        /// Генерирует MD5-хеш
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetHash(string value)
        {
            MD5 md5Hasher = MD5.Create();
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(value));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                // Записываем данные в 16-ричном формате
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }
    }
}
