﻿using FaceFind.Entities.BO;
using System;
using System.Collections.Generic;

namespace FaceFindBll.Interfaces
{
    internal interface IUserLogic
    {
        void AddNewUser(User user, string name, string surname);

        void AddUserRole(string login, string role);

        bool CanAuthenticate(string login, string password);

        void ChangePassword(string login, string newPassword);
        void DeleteUserRole(string login, string role);
        IEnumerable<Role> GetRolesForUser(string login);
        User GetUserById(Guid id);
        User GetUserByLogin(string login);
        bool IsLoginExists(string login);
        bool IsUserInRole(string login, string role);
        void BanPerson(Guid personId);
        void UnbanPerson(Guid personId);
        IEnumerable<Guid> GetUsersIdByRole(string role);
    }
}
