﻿using FaceFind.Entities;
using FaceFind.Entities.BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceFindBll.Interfaces
{
    internal interface IMessagesLogic
    {
        IEnumerable<Dialog> GetUserDialogs(string UserLogin);
        Dialog GetUserDialog(string userLogin, Guid dialogId);
        void SendMessage(Message msg, Guid dialogId);

    }
}
