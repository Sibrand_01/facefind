﻿using FaceFind.Entities.BO;
using System;
using System.Collections.Generic;


namespace FaceFindBll.Interfaces
{
    internal interface IPersonLogic
    {
        void UpdatePerson(Person person);
        Person GetPersonById(Guid id);
        Person GetPersonByUserLogin(string userLogin);
        IEnumerable<Person> GetAllPeople();
        IEnumerable<Person> GetUserFollowers(Guid personId);
        IEnumerable<Person> GetFollowedPersons(Guid personId);
        void FollowPerson(Guid personId, Guid followerId);
        void UnfollowPerson(Guid personId, Guid followerId);
    }
}
