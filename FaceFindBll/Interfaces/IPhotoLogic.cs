﻿using FaceFind.Entities.BO;
using System;

namespace FaceFindBll.Interfaces
{
    internal interface IPhotoLogic
    {
        void AddOrChangePhoto(Photo photo);
        void DeletePhoto(Guid id);
        Photo GetUserPhoto(Guid userId);
    }
}
