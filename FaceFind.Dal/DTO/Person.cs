﻿using System;
using System.Collections.Generic;

namespace FaceFind.Entities.DTO
{
    /// <summary>
    /// Основная информация о человеке
    /// </summary>
    public class Person
    {

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string SecondName { get; set; }
        public DateTime? BirthDay { get; set; }
        public string Status { get; set; }


    }
}