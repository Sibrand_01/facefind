﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceFind.Entities.DTO
{
    /// <summary>
    /// Kласс, описывающий фотографию
    /// </summary>
    public class Photo
    {
        public Guid Id { get; set; }
        public byte[] Content { get; set; }
        public string ContentType { get; set; }
    }
}
