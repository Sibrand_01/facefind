﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FaceFind.Entities.DTO
{
    /// <summary>
    /// Класс описывает сообщение.
    /// </summary>
    public class Message
    {
        public Guid Id { get; set; } 
        public Guid SenderId { get; set; }
        public DateTime SendDate { get; set; }
        public bool IsSeen { get; set; }
        public string Content { get; set; }

    }
}