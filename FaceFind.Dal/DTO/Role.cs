﻿using System;

namespace FaceFind.Entities.DTO
{
    public class Role
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
    }
}
