﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FaceFind.Entities.DTO
{
    /// <summary>
    /// Класс описывает диалог пользователей.
    /// </summary>
    public class Dialog
    {
        public Guid Id { get; set; }
        public Guid Participant1Id { get; set; }
        public Guid Participant2Id { get; set; }
    }
}