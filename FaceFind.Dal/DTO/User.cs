﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceFind.Entities.DTO
{
    /// <summary>
    /// Модель пользователя, содержащая основную информацию для авторизации
    /// </summary>
    public class User
    {        
        public string Login { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public string PasswordHash { get; set; }
        public DateTime? PasswordChangeDate { get; set; }
        public Guid Id { get; set;}
    }
}
