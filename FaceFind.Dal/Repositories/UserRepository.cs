﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Common;
using FaceFind.Entities.DTO;

namespace FaceFind.Dal.Repositories
{
    internal class UserRepository : BaseRepository, IUserRepository
    {
        public void AddNewUser(User user, string name, string surname)
        {
            using (var cmd = new SqlCommand(StoredProcedures.User.CreateUser))
            {
                cmd.Parameters.AddWithValue(StoredProcedures.User.Params.UserLogin, user.Login);
                cmd.Parameters.AddWithValue(StoredProcedures.User.Params.Name, name);
                cmd.Parameters.AddWithValue(StoredProcedures.User.Params.UserLogin, surname);
                cmd.Parameters.AddWithValue(StoredProcedures.User.Params.UserLogin, surname);
                ExecuteNonQuery(cmd);
            }
        }

        public void AddRoleToUser(string login, string role)
        {
            using (var cmd = new SqlCommand(StoredProcedures.User.AddUserToRoleByLogin))
            {
                cmd.Parameters.AddWithValue(StoredProcedures.User.Params.UserLogin, login);
                cmd.Parameters.AddWithValue(StoredProcedures.User.Params.Role, role);
                ExecuteNonQuery(cmd);
            }
        }

        public void AddRoleToUserById(Guid id, string role)
        {
            using (var cmd = new SqlCommand(StoredProcedures.User.AddUserToRole))
            {
                cmd.Parameters.AddWithValue(StoredProcedures.User.Params.UserId, id);
                cmd.Parameters.AddWithValue(StoredProcedures.User.Params.Role, role);
                ExecuteNonQuery(cmd);
            }
        }

        public void ChangeUserPassword(string login, string newPassword)
        {
            using (var cmd = new SqlCommand(StoredProcedures.User.ChangePassword))
            {
                cmd.Parameters.AddWithValue(StoredProcedures.User.Params.UserLogin, login);
                cmd.Parameters.AddWithValue(StoredProcedures.User.Params.PasswordHash, newPassword);
                ExecuteNonQuery(cmd);
            }
        }

        public void DeleteUserFromRole(string login, string role)
        {
            using (var cmd = new SqlCommand(StoredProcedures.User.RemoveUserRole))
            {
                cmd.Parameters.AddWithValue(StoredProcedures.User.Params.UserLogin, login);
                cmd.Parameters.AddWithValue(StoredProcedures.User.Params.Role, role);
                ExecuteNonQuery(cmd);
            }
        }

        public void DeleteUserFromRoleById(Guid id, string role)
        {
            using (var cmd = new SqlCommand(StoredProcedures.User.RemoveUserRoleById))
            {
                cmd.Parameters.AddWithValue(StoredProcedures.User.Params.UserId, id);
                cmd.Parameters.AddWithValue(StoredProcedures.User.Params.Role, role);
                ExecuteNonQuery(cmd);
            }
        }

        public IEnumerable<Role> GetRolesForUser(string login)
        {
            using (var cmd = new SqlCommand(StoredProcedures.User.GetRolesForUser))
            {
                var res = new List<Role>();
                cmd.Parameters.AddWithValue(StoredProcedures.User.Params.UserLogin, login);
                ExecuteReader(cmd, (reader) =>
                {
                    res.Add(RoleFromReader(reader));
                });
                return res;
            }
        }

        public User GetUserById(Guid id)
        {
            using (var cmd = new SqlCommand(StoredProcedures.User.GetUserByID))
            {
                User res = null;
                cmd.Parameters.AddWithValue(StoredProcedures.User.Params.UserId, id);
                ExecuteReader(cmd, (reader) =>
                {
                    res = UserFromReader(reader);
                });
                return res;
            }
        }

        public User GetUserByLogin(string login)
        {
            using (var cmd = new SqlCommand(StoredProcedures.User.GetUserByLogin))
            {
                User res = null;
                cmd.Parameters.AddWithValue(StoredProcedures.User.Params.UserLogin, login);
                ExecuteReader(cmd, (reader) =>
                {
                    res = UserFromReader(reader);
                });
                return res;
            }
        }

        public IEnumerable<Guid> GetUserIdsByRole(string role)
        {
            using (var cmd = new SqlCommand(StoredProcedures.User.GetUserIdsByRole))
            {
                var res = new List<Guid>();
                cmd.Parameters.AddWithValue(StoredProcedures.User.Params.Role, role);
                ExecuteReader(cmd, (reader) =>
                {
                    res.Add((Guid)reader["ID"]);
                });
                return res;
            }
        }

        public bool IsLoginExists(string login)
        {
            using (var cmd = new SqlCommand(StoredProcedures.User.GetUserIdsByRole))
            {
                cmd.Parameters.AddWithValue(StoredProcedures.User.Params.UserLogin, login);
                return ExecuteScalar<bool>(cmd);
            }
        }

        public bool IsUserInRole(string login, string role)
        {
            using (var cmd = new SqlCommand(StoredProcedures.User.GetUserIdsByRole))
            {
                cmd.Parameters.AddWithValue(StoredProcedures.User.Params.UserLogin, login);
                cmd.Parameters.AddWithValue(StoredProcedures.User.Params.Role, role);
                return ExecuteScalar<bool>(cmd);
            }
        }

        private static Role RoleFromReader(SqlDataReader reader)
        {
            return new Role
            {
                Id = (Guid)reader["ID"],
                Title = reader["Title"].ToNullSafeString()
            };
        }
        private static User UserFromReader(SqlDataReader reader)
        {
            return new User
            {
                Id = (Guid)reader["ID"],
                Login = reader["Login"].ToNullSafeString(),
                PasswordHash = reader["PasswordHash"].ToNullSafeString(),
                PasswordChangeDate = reader["PasswordChangeDate"] == null ? (DateTime?)null : DateTime.Parse(reader["PasswordChangeDate"].ToString()),
                RegistrationDate = DateTime.Parse(reader["RegistrationDate"].ToString())
            };
        }
    }
}
