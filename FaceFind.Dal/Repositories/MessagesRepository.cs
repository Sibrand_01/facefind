﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using Common;
using FaceFind.Entities.DTO;

namespace FaceFind.Dal.Repositories
{
    internal class MessagesRepository : BaseRepository, IMessageRepository
    {
        public void AddMessage(Message message, Guid dialogId)
        {
            ArgumentGuard.ThrowOnNull(message, nameof(message));
            ArgumentGuard.ThrowOnStringIsNullOrEmpty(message.Content, "Content");
            using (var cmd = new SqlCommand(StoredProcedures.Messages.SendMessage))
            {
                cmd.Parameters.AddWithValue(StoredProcedures.Messages.Params.Content, message.Content);
                cmd.Parameters.AddWithValue(StoredProcedures.Messages.Params.DialogId, dialogId);
                cmd.Parameters.AddWithValue(StoredProcedures.Messages.Params.SenderId, message.SenderId);
                ExecuteNonQuery(cmd);
            }
        }

        public Dialog GetDialog(Guid dialogId)
        {
            using (var cmd = new SqlCommand(StoredProcedures.Messages.GetUserDialogs))
            {
                Dialog res = null;
                cmd.Parameters.AddWithValue(StoredProcedures.Messages.Params.DialogId, dialogId);
                ExecuteReader(cmd, (reader) =>
                {
                    res = DialogFromReader(reader);
                });
                return res;
            }
        }

        public Message GetLastMessage(Guid dialogId)
        {
            using (var cmd = new SqlCommand(StoredProcedures.Messages.GetUserDialogs))
            {
                Message res = null;
                cmd.Parameters.AddWithValue(StoredProcedures.Messages.Params.DialogId, dialogId);
                ExecuteReader(cmd, (reader) =>
                {
                    res = MessageFromReader(reader);
                });
                return res;
            }
        }

        public IEnumerable<Message> GetMessages(Guid dialogId)
        {
            using (var cmd = new SqlCommand(StoredProcedures.Messages.GetUserDialogs))
            {
                var res = new List<Message>();
                cmd.Parameters.AddWithValue(StoredProcedures.Messages.Params.DialogId, dialogId);
                ExecuteReader(cmd, (reader) =>
                {
                    res.Add(MessageFromReader(reader));
                });
                return res;
            }
        }

        public IEnumerable<Dialog> GetUserDialogs(string userLogin)
        {
            using (var cmd = new SqlCommand(StoredProcedures.Messages.GetUserDialogs))
            {
                var res = new List<Dialog>();
                cmd.Parameters.AddWithValue(StoredProcedures.Messages.Params.UserLogin, userLogin);
                ExecuteReader(cmd, (reader) =>
                {
                    res.Add(DialogFromReader(reader));
                });
                return res;
            }
        }

        private static Message MessageFromReader(SqlDataReader reader)
        {
            return new Message
            {
                Id = Guid.Parse(reader["Id"].ToNullSafeString()),
                SenderId = (Guid)reader["SenderId"],
                Content = Encoding.UTF8.GetString((byte[])reader["Content"]),
                IsSeen = (bool)reader["IsSeen"],
                SendDate = DateTime.Parse(reader["SendDate"].ToNullSafeString())
            };
        }
        private static Dialog DialogFromReader(SqlDataReader reader)
        {
            return new Dialog
            {
                Id = Guid.Parse(reader["Id"].ToNullSafeString()),
                Participant1Id = Guid.Parse(reader["Participant1_Id"].ToNullSafeString()),
                Participant2Id = Guid.Parse(reader["Participant2_Id"].ToNullSafeString())
            };
        }
    }
}
