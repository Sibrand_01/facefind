﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FaceFind.Entities;
using System.Data.SqlClient;
using Common;
using FaceFind.Entities.DTO;

namespace FaceFind.Dal.Repositories
{
    internal class PhotoRepository : BaseRepository, IPhotoRepository
    {
        public Photo GetPersonPhoto(Guid personId)
        {
            using (var cmd = new SqlCommand(StoredProcedures.Photo.GetPersonPhoto))
            {
                Photo res = null;
                cmd.Parameters.AddWithValue(StoredProcedures.Photo.Params.UserId, personId);
                ExecuteReader(cmd, (reader) =>
                {
                    res = new Photo
                    {
                        Id = (Guid)reader["ID"],
                        Content = (byte[])reader["Content"],
                        ContentType = reader["Content Type"].ToNullSafeString()
                    };
                });
                return res;
            }
        }

        public void RemovePhoto(Guid Id)
        {
            using (var cmd = new SqlCommand(StoredProcedures.Photo.RemovePhoto))
            {
                cmd.Parameters.AddWithValue(StoredProcedures.Photo.Params.UserId, Id);
                ExecuteNonQuery(cmd);
            }
        }

        public void UploadOrUpdate(Photo photo)
        {
            using (var cmd = new SqlCommand(StoredProcedures.Photo.UploadOrUpdatePhoto))
            {
                cmd.Parameters.AddWithValue(StoredProcedures.Photo.Params.UserId, photo.Id);
                cmd.Parameters.AddWithValue(StoredProcedures.Photo.Params.ContentType, photo.ContentType ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue(StoredProcedures.Photo.Params.Content, photo.Content ?? (object)DBNull.Value);
                ExecuteNonQuery(cmd);
            }
        }
    }
}
