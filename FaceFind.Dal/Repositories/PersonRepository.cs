﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Common;
using FaceFind.Entities.DTO;

namespace FaceFind.Dal.Repositories
{
    internal class PersonRepository : BaseRepository, IPersonRepository
    {
        public void FollowPerson(Guid personId, Guid followerId)
        {
            using (var cmd = new SqlCommand(StoredProcedures.Person.FollowPerson))
            {
                cmd.Parameters.AddWithValue(StoredProcedures.Person.Params.FollowerId, followerId);
                cmd.Parameters.AddWithValue(StoredProcedures.Person.Params.UserId, personId);
                ExecuteNonQuery(cmd);
            }
        }
        public void UnfollowPerson(Guid personId, Guid followerId)
        {
            using (var cmd = new SqlCommand(StoredProcedures.Person.UnFollowPerson))
            {
                cmd.Parameters.AddWithValue(StoredProcedures.Person.Params.FollowerId, followerId);
                cmd.Parameters.AddWithValue(StoredProcedures.Person.Params.UserId, personId);
                ExecuteNonQuery(cmd);
            }
        }

        public IEnumerable<Person> GetAllPeople()
        {
            using (var cmd = new SqlCommand(StoredProcedures.Person.GetPeople))
            {
                var res = new List<Person>();
                ExecuteReader(cmd, (reader)=>
                {
                    res.Add(PersonFromReader(reader));
                });
                return res;
            }
        }

        public IEnumerable<Person> GetFollowed(Guid personId)
        {
            using (var cmd = new SqlCommand(StoredProcedures.Person.GetFollowed))
            {
                cmd.Parameters.AddWithValue(StoredProcedures.Person.Params.UserId, personId);
                var res = new List<Person>();
                ExecuteReader(cmd, (reader) =>
                {
                    res.Add(PersonFromReader(reader));
                });
                return res;
            }
        }

        public IEnumerable<Person> GetFollowers(Guid personId)
        {
            using (var cmd = new SqlCommand(StoredProcedures.Person.GetFollowers))
            {
                cmd.Parameters.AddWithValue(StoredProcedures.Person.Params.UserId, personId);
                var res = new List<Person>();
                ExecuteReader(cmd, (reader) =>
                {
                    res.Add(PersonFromReader(reader));
                });
                return res;
            }
        }

        public Person GetPersonById(Guid id)
        {
            using (var cmd = new SqlCommand(StoredProcedures.Person.GetPersonByID))
            {
                cmd.Parameters.AddWithValue(StoredProcedures.Person.Params.UserId, id);
                Person res = null;
                ExecuteReader(cmd, (reader) =>
                {
                    res = PersonFromReader(reader);
                });
                return res;
            }
        }

        public Person GetPersonByUserLogin(string login)
        {
            using (var cmd = new SqlCommand(StoredProcedures.Person.GetPersonByLogin))
            {
                cmd.Parameters.AddWithValue(StoredProcedures.Person.Params.UserLogin, login);
                Person res = null;
                ExecuteReader(cmd, (reader) =>
                {
                    res = PersonFromReader(reader);
                });
                return res;
            }
        }

        public void UpdatePerson(Person person)
        {
            using (var cmd = new SqlCommand(StoredProcedures.Person.GetPersonByLogin))
            {
                cmd.Parameters.AddWithValue(StoredProcedures.Person.Params.UserId, person.Id);
                cmd.Parameters.AddWithValue(StoredProcedures.Person.Params.Name, string.IsNullOrEmpty(person.Name) ? (object)DBNull.Value : person.Name);
                cmd.Parameters.AddWithValue(StoredProcedures.Person.Params.Surname, string.IsNullOrEmpty(person.Surname) ? (object)DBNull.Value : person.Surname);
                cmd.Parameters.AddWithValue(StoredProcedures.Person.Params.BirthDay, person.BirthDay ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue(StoredProcedures.Person.Params.Status, string.IsNullOrEmpty(person.Status) ? (object)DBNull.Value : person.Status);
                ExecuteNonQuery(cmd);
            }
        }

        private static Person PersonFromReader(SqlDataReader reader)
        {
            return new Person
            {
                Id = (Guid)reader["ID"],
                Name = reader["Name"].ToNullSafeString(),
                Surname = reader["Surname"].ToNullSafeString(),
                BirthDay = DateTime.Parse(reader["BirthDat"].ToNullSafeString()),
                SecondName = reader["SecondName"].ToNullSafeString(),
                Status = reader["Status"].ToNullSafeString()
            };
        }
    }
}
