﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceFind.Dal.Repositories
{
    public interface IBaseRepository
    {
    }

    abstract class BaseRepository:IBaseRepository
    {
        protected readonly string _connectionString;
        protected BaseRepository()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["Main"].ConnectionString;
        }

        protected virtual void ExecuteNonQuery(SqlCommand cmd)
        {
            using (var sqlCon = new SqlConnection(_connectionString))
            {
                cmd.Connection = sqlCon;
                sqlCon.Open();
                cmd.ExecuteNonQuery();
            }
        }
        protected virtual T ExecuteScalar<T>(SqlCommand cmd)
        {
            using (var sqlCon = new SqlConnection(_connectionString))
            {
                cmd.Connection = sqlCon;
                sqlCon.Open();
                return (T)cmd.ExecuteScalar();
            }
        }

        protected virtual void ExecuteReader(SqlCommand cmd, Action<SqlDataReader> func)
        {
            using (var sqlCon = new SqlConnection(_connectionString))
            {
                cmd.Connection = sqlCon;
                sqlCon.Open();
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    func?.Invoke(reader);
                }
            }
        }
    }
}
