﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceFind.Dal.StoredProcedures
{
    internal static class User
    {
        public static readonly string CreateUser = "CreateUser";
        public static readonly string ChangePassword = "ChangePassword";
        public static readonly string AddUserToRole = "AddUserToRole";
        public static readonly string AddUserToRoleByLogin = "AddUserToRoleByLogin";
        public static readonly string GetUserByID = "GetUserById";
        public static readonly string GetUserByLogin = "GetUserByLogin";
        public static readonly string GetUsers = "GetUsers";
        public static readonly string GetUserIdsByRole = "GetUserIdsByRole";
        public static readonly string isUserInRole = "isUserInRole";
        public static readonly string RemoveUserRole = "RemoveUserRole";
        public static readonly string RemoveUserRoleById = "RemoveUserRoleById";
        public static readonly string GetRolesForUser = "GetRolesForUser";

        public static class Params
        {
            public static readonly string Name = "@name";
            public static readonly string Surname = "@surname";
            public static readonly string PasswordHash = "@pwd";
            public static readonly string UserLogin = "@login";
            public static readonly string Role = "@roleTitle";
            public static readonly string UserId = "@userId";
        }
    }

    internal static class Person
    {
        public static readonly string FollowPerson = "FollowPerson";
        public static readonly string UnFollowPerson = "UnFollowPerson";
        public static readonly string GetFollowers = "GetFollowers";
        public static readonly string GetFollowed = "GetFollowed";
        public static readonly string GetPersonByID = "GetPersonByID";
        public static readonly string GetPersonByLogin = "GetPersonByUserLogin";
        public static readonly string GetPeople = "GetPeople";
        public static readonly string UpdatePerson = "UpdatePerson";

        public static class Params
        {
            public static readonly string BirthDay = "@bday";
            public static readonly string FollowerId = "@followerId";
            public static readonly string Name = "@name";
            public static readonly string SecondName = "@secondName";
            public static readonly string Status = "@status";
            public static readonly string Surname = "@surname";
            public static readonly string UserId = "@personId";
            public static readonly string UserLogin = "@login";
        }
    }

    internal static class Photo
    {
        public static readonly string UploadOrUpdatePhoto = "UploadOrUpdatePhoto";
        public static readonly string RemovePhoto = "RemovePhoto";
        public static readonly string GetPersonPhoto = "GetPersonPhoto";

        public static class Params
        {
            public static readonly string Content = "@content";
            public static readonly string ContentType = "@contentType";
            public static readonly string UserId = "@personId";
        }
    }

    internal static class Messages
    {
        public static readonly string GetDialog = "GetDialog";
        public static readonly string GetUserDialogs = "GetUserDialogs";
        public static readonly string SendMessage = "SendMessage";
        public static readonly string GetLastMessage = "GetLastMessage";
        public static readonly string GetMessages = "GetMessages";

        public static class Params
        {
            public static readonly string Content = "@content";
            public static readonly string DialogId = "@dialogId";
            public static readonly string SenderId = "@senderId";
            public static readonly string UserLogin = "@userLogin";
        }
    }
}
