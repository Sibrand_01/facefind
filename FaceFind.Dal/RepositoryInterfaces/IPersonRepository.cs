﻿using FaceFind.Dal.Repositories;
using FaceFind.Entities.DTO;
using System;
using System.Collections.Generic;

namespace FaceFind.Dal
{
    public interface IPersonRepository : IBaseRepository
    {
        void UpdatePerson(Person person);//+
        Person GetPersonByUserLogin(string login);//+
        Person GetPersonById(Guid id); //+
        IEnumerable<Person> GetAllPeople(); //+
        void FollowPerson(Guid personId, Guid followerId);//+
        void UnfollowPerson(Guid personId, Guid followerId);//+
        IEnumerable<Person> GetFollowers(Guid personId);//+
        IEnumerable<Person> GetFollowed(Guid personId);//+
    }
}
