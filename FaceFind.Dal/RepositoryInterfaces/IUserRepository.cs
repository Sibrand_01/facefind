﻿using FaceFind.Dal.Repositories;
using FaceFind.Entities.DTO;
using System;
using System.Collections.Generic;

namespace FaceFind.Dal
{
    public interface IUserRepository : IBaseRepository
    {
        void AddNewUser(User user, string name, string surname); //+
        bool IsLoginExists(string login); //+
        bool IsUserInRole(string login, string role);//+
        void AddRoleToUser(string login, string role);//+
        void AddRoleToUserById(Guid id, string role);//+
        void DeleteUserFromRole(string login, string role);// +
        void DeleteUserFromRoleById(Guid id, string role);// +
        void ChangeUserPassword(string login, string newPassword); //+
        User GetUserById(Guid id); //+
        User GetUserByLogin(string login); //+
        IEnumerable<Guid> GetUserIdsByRole(string role); //+
        IEnumerable<Role> GetRolesForUser(string login);

    }
}
