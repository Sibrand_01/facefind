﻿using FaceFind.Dal.Repositories;
using FaceFind.Entities.DTO;
using System;
using System.Collections.Generic;

namespace FaceFind.Dal
{
    public interface IMessageRepository: IBaseRepository
    {
        IEnumerable<Dialog> GetUserDialogs(string userLogin); //+
        Dialog GetDialog(Guid dialogId);
        void AddMessage(Message message, Guid dialogId);//+
        Message GetLastMessage(Guid dialogId);//-
        IEnumerable<Message> GetMessages(Guid dialogId);//-
    }
}
