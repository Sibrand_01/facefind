﻿using FaceFind.Dal.Repositories;
using FaceFind.Entities.DTO;
using System;

namespace FaceFind.Dal
{
    public interface IPhotoRepository : IBaseRepository
    {
        void UploadOrUpdate(Photo photo);//+
        Photo GetPersonPhoto(Guid personId);//+
        void RemovePhoto(Guid Id); //+
    }
}
