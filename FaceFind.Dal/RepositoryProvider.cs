﻿using FaceFind.Dal.Repositories;
using System;
using System.Collections.Generic;

namespace FaceFind.Dal
{
    public class RepositoryProvider
    {
        static Dictionary<Type, object> providers = new Dictionary<Type, object>();

        public static T GetRepository<T>()
            where T : class, IBaseRepository
        {
            if (!providers.ContainsKey(typeof(T)))
            {
                throw new InvalidOperationException("Provider not registered");
            }
            return providers[typeof(T)] as T;
        }

        static void RegisterRepository<T>(Type type, T repository)
            where T : IBaseRepository
        {
            providers[type] = repository;
        }

        static void RegisterRepository<T>(T repository)
            where T : IBaseRepository
        {
            providers[typeof(T)] = repository;
        }


        static RepositoryProvider()
        {
            //Register default providers
            RegisterRepository<IPhotoRepository>(new PhotoRepository());
            RegisterRepository<IUserRepository>(new UserRepository());
            RegisterRepository<IPersonRepository>(new PersonRepository());
            RegisterRepository<IMessageRepository>(new MessagesRepository());

        }
    }
}
