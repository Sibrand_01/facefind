﻿using FaceFind.Dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;


namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            var cols = new List<string>(){"aa", "bb", "cc"};
            var modelValues = new List<object>() {"aa", DateTime.Now, null};
            var cmd = new SqlCommand();
            var insert = CommandBuilder.GetInsertCommand(Tables.Dialogs, cols, modelValues, ref cmd).Where(ref cmd, "ba", 1).Where(ref cmd, "gg", 3, false);
            Console.WriteLine(insert.ToString());
            Console.WriteLine(string.Join(",",cmd.Parameters.Cast<SqlParameter>().Select(p=>p.ParameterName)));

            cmd = new SqlCommand();
            var select = CommandBuilder.GetSelectCommand(Tables.Messages, cols).Join(Tables.People, "qq", "qq2", new[] { "ccc", "aca", "bbq" }).Top(26).Where(ref cmd, "ID", "zzz");
            Console.WriteLine(select.ToString());
            Console.WriteLine(string.Join(",", cmd.Parameters.Cast<SqlParameter>().Select(p => p.ParameterName)));

            cmd = new SqlCommand();
            var delete = CommandBuilder.GetDeleteCommand(Tables.PersonPersons).Where(ref cmd, "aa", 11);

            Console.WriteLine(delete.ToString());
            Console.WriteLine(string.Join(",", cmd.Parameters.Cast<SqlParameter>().Select(p => p.ParameterName)));

            cmd = new SqlCommand();
            var update = CommandBuilder.GetUpdateCommand(Tables.PersonPersons, cols, modelValues, ref cmd).Where(ref cmd, "aa", 11);

            Console.WriteLine(update.ToString());
            Console.WriteLine(string.Join(",", cmd.Parameters.Cast<SqlParameter>().Select(p => p.ParameterName)));

            Console.ReadKey();
        }
    }
}
