USE [master]
GO
/****** Object:  Database [FaceFind.Models.FaceFindDbContext]    Script Date: 6/29/2016 5:21:31 PM ******/
CREATE DATABASE [FaceFind.Models.FaceFindDbContext]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'FaceFind.Models.FaceFindDbContext', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\FaceFind.Models.FaceFindDbContext.mdf' , SIZE = 4288KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'FaceFind.Models.FaceFindDbContext_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\FaceFind.Models.FaceFindDbContext_log.ldf' , SIZE = 1088KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [FaceFind.Models.FaceFindDbContext] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [FaceFind.Models.FaceFindDbContext].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [FaceFind.Models.FaceFindDbContext] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [FaceFind.Models.FaceFindDbContext] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [FaceFind.Models.FaceFindDbContext] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [FaceFind.Models.FaceFindDbContext] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [FaceFind.Models.FaceFindDbContext] SET ARITHABORT OFF 
GO
ALTER DATABASE [FaceFind.Models.FaceFindDbContext] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [FaceFind.Models.FaceFindDbContext] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [FaceFind.Models.FaceFindDbContext] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [FaceFind.Models.FaceFindDbContext] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [FaceFind.Models.FaceFindDbContext] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [FaceFind.Models.FaceFindDbContext] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [FaceFind.Models.FaceFindDbContext] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [FaceFind.Models.FaceFindDbContext] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [FaceFind.Models.FaceFindDbContext] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [FaceFind.Models.FaceFindDbContext] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [FaceFind.Models.FaceFindDbContext] SET  ENABLE_BROKER 
GO
ALTER DATABASE [FaceFind.Models.FaceFindDbContext] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [FaceFind.Models.FaceFindDbContext] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [FaceFind.Models.FaceFindDbContext] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [FaceFind.Models.FaceFindDbContext] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [FaceFind.Models.FaceFindDbContext] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [FaceFind.Models.FaceFindDbContext] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [FaceFind.Models.FaceFindDbContext] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [FaceFind.Models.FaceFindDbContext] SET RECOVERY FULL 
GO
ALTER DATABASE [FaceFind.Models.FaceFindDbContext] SET  MULTI_USER 
GO
ALTER DATABASE [FaceFind.Models.FaceFindDbContext] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [FaceFind.Models.FaceFindDbContext] SET DB_CHAINING OFF 
GO
ALTER DATABASE [FaceFind.Models.FaceFindDbContext] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [FaceFind.Models.FaceFindDbContext] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'FaceFind.Models.FaceFindDbContext', N'ON'
GO
USE [FaceFind.Models.FaceFindDbContext]
GO
/****** Object:  StoredProcedure [dbo].[AddUserToRole]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
CREATE PROCEDURE [dbo].[AddUserToRole] 
	@userId uniqueidentifier,
	@roleTitle nvarchar(max) 
AS
BEGIN
	insert into UserRoles (UserID, RoleID) 
	values (@userId, (select ID from Roles where Title = @roleTitle))
END

GO
/****** Object:  StoredProcedure [dbo].[AddUserToRoleByLogin]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[AddUserToRoleByLogin]
	@login nvarchar(max),
	@roleTitle nvarchar(max)
AS
BEGIN
	insert into UserRoles (UserID, RoleID)
	values ((select ID from Users where Login = @login), (select ID from Roles where Title = @roleTitle))
END

GO
/****** Object:  StoredProcedure [dbo].[ChangePassword]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[ChangePassword]
	@pwd nvarchar(max),
	@login nvarchar(max)
AS
BEGIN
	update Users
	set PasswordHash = @pwd,
	PasswordChangeDate = GETDATE()
	where Login = @login
END

GO
/****** Object:  StoredProcedure [dbo].[CreateUser]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CreateUser] 
	@login nvarchar(max),
	@pwd nvarchar(max),
	@name nvarchar(max),
	@surname nvarchar(max)
AS
BEGIN
	insert into Users (Login, PasswordHash, RegistrationDate)
	values (@Login, @pwd, getdate())

	insert into People (ID,Name,Surname)
	values ((select ID from Users where Login = @Login),@name,@surname)

	insert into UserRoles (UserID,RoleID)
	values((select ID from Users where Login = @Login),
	(select ID from Roles where Title = N'User'))

END

GO
/****** Object:  StoredProcedure [dbo].[FollowPerson]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[FollowPerson]
	@personId uniqueidentifier,
	@followerId uniqueidentifier
AS
BEGIN
	insert into PersonFollowers (Person_ID,Follower_ID)
	values(@personId,@followerId)
END

GO
/****** Object:  StoredProcedure [dbo].[GetDialog]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetDialog]
	@personId uniqueidentifier,
	@receiverId uniqueidentifier,
	@dialogId uniqueidentifier
AS
BEGIN
	if exists(select * from Dialogs where ID = @dialogId)
	select * from Dialogs where ID = @dialogId
	else 
	 insert into Dialogs (ID, Participant1_ID, Participant2_ID) values (@dialogId,@personId,@receiverId)
	 select * from Dialogs where ID = @dialogId

END

GO
/****** Object:  StoredProcedure [dbo].[GetFollowed]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[GetFollowed]
	@personId uniqueidentifier
AS
BEGIN
	select * from PersonFollowers
	join People as p on Person_ID = p.ID
	where Follower_ID = @personId
END

GO
/****** Object:  StoredProcedure [dbo].[GetFollowers]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[GetFollowers]
	@personId uniqueidentifier
AS
BEGIN
	select * from PersonFollowers
	join People as p on Follower_ID = p.ID
	where Person_ID = @personId
END

GO
/****** Object:  StoredProcedure [dbo].[GetLastMessage]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetLastMessage]
	@dialogId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;
	SELECT top 1 * from Messages
	where Dialog_ID = @dialogId
	order by SendDate desc
END

GO
/****** Object:  StoredProcedure [dbo].[GetMessages]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetMessages]
	@dialogId uniqueidentifier
AS
BEGIN
	SELECT * from Messages
	where Dialog_ID = @dialogId
	order by SendDate desc
END

GO
/****** Object:  StoredProcedure [dbo].[GetPeople]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[GetPeople]
AS
BEGIN
	select * from People
END

GO
/****** Object:  StoredProcedure [dbo].[GetPersonById]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[GetPersonById]
	@personId uniqueidentifier
AS
BEGIN
	select * from People
	where ID = @personId
END

GO
/****** Object:  StoredProcedure [dbo].[GetPersonByUserLogin]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[GetPersonByUserLogin]
	@login nvarchar(max)
AS
BEGIN
	select * from People
	where ID in (select ID from Users where Login = @login)
END

GO
/****** Object:  StoredProcedure [dbo].[GetPersonPhoto]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetPersonPhoto]
	@personId uniqueidentifier
AS
BEGIN
	select * from Photos where ID = @personId
END

GO
/****** Object:  StoredProcedure [dbo].[GetRolesByTitle]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetRolesByTitle]
	@roleTitle nvarchar(200)
AS
BEGIN
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from Roles where Title = @roleTitle
END

GO
/****** Object:  StoredProcedure [dbo].[GetRolesForUser]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetRolesForUser]
	-- Add the parameters for the stored procedure here
	@login nvarchar(max)
AS
BEGIN
	select r.ID, r.Title from UserRoles
	join Roles as r on RoleID = r.ID
	join Users as u on UserID = u.ID
	where u.Login = @login
END

GO
/****** Object:  StoredProcedure [dbo].[GetUserById]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[GetUserById]
	@userId uniqueidentifier
AS
BEGIN
	select * from Users
	where ID = @userId
END

GO
/****** Object:  StoredProcedure [dbo].[GetUserByLogin]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[GetUserByLogin]
	@login nvarchar(max)
AS
BEGIN
	select * from Users
	where Login =  @login
END

GO
/****** Object:  StoredProcedure [dbo].[GetUserDialogs]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetUserDialogs]
	@userLogin uniqueidentifier
AS
BEGIN
	select * from Dialogs
	join Users as u1 on Participant1_ID = u1.ID
	join Users as u2 on Participant2_ID = u2.ID
	where u1.Login = @userLogin or u2.Login = @userLogin

END

GO
/****** Object:  StoredProcedure [dbo].[GetUserIdsByRole]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[GetUserIdsByRole]
	@roleTitle nvarchar(max)
AS
BEGIN
	select UserID from UserRoles
	join Roles as r on RoleID = r.Id
	where r.Title = @roleTitle
END

GO
/****** Object:  StoredProcedure [dbo].[GetUsers]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[GetUsers]
AS
BEGIN
	select * from Users
END

GO
/****** Object:  StoredProcedure [dbo].[IsLoginExists]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[IsLoginExists]
	@login nvarchar(max)
AS
BEGIN
	select case when exists (select ID from Users where Login = @login) then cast(1 as bit) else cast(0 as bit) end
	from Users
END

GO
/****** Object:  StoredProcedure [dbo].[isUserInRole]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[isUserInRole]
	@roleTitle nvarchar(max),
	@login nvarchar(max)
AS
BEGIN
	select 
	case when exists(select UserID from UserRoles 
	join Roles as r on RoleID = r.Id 
	join Users as u on UserID = u.Id
	where Title = @roleTitle and Login = @login) 
	then cast(1 as bit)
	else cast(0 as bit) 
	end
	from UserRoles 	
END

GO
/****** Object:  StoredProcedure [dbo].[RemovePhoto]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[RemovePhoto]
	@personId uniqueidentifier
AS
BEGIN
	delete from Photos where Id = @personId
END

GO
/****** Object:  StoredProcedure [dbo].[RemoveUserRole]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[RemoveUserRole]
	@Login nvarchar(max),
	@roleTitle nvarchar(max)
AS
BEGIN
	delete from UserRoles
	where UserID in (select ID from Users where Login = @Login) and RoleID in (select ID from Roles where Title = @roleTitle)
END

GO
/****** Object:  StoredProcedure [dbo].[RemoveUserRoleById]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RemoveUserRoleById]
	-- Add the parameters for the stored procedure here
	@userId uniqueidentifier,
	@roleTitle nvarchar(max)
AS
BEGIN
	insert into UserRoles (UserID, RoleID)
	values (@userId, (select ID from Roles where Title = @roleTitle))
END

GO
/****** Object:  StoredProcedure [dbo].[SendMessage]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SendMessage]
	@dialogId uniqueidentifier,
	@senderId uniqueidentifier,
	@content nvarchar(max)
AS
BEGIN	
	 insert into Messages(Dialog_ID,From_Id,Content,SendDate,IsSeen) 
	 values (@dialogId,@senderId,@content, getdate(),0)
END

GO
/****** Object:  StoredProcedure [dbo].[UnFollowPerson]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[UnFollowPerson]
	@personId uniqueidentifier,
	@followerId uniqueidentifier
AS
BEGIN
	delete from PersonFollowers
	where Person_Id = @personId and Follower_Id = @followerId
END

GO
/****** Object:  StoredProcedure [dbo].[UpdatePerson]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[UpdatePerson]
	@personId uniqueidentifier,
	@name nvarchar(max),
	@surname nvarchar(max),
	@secondName nvarchar(max),
	@bday datetime,
	@status nvarchar(max)
AS
BEGIN
	update People
	set Name = @name,
	Surname = @surname,
	SecondName = @secondName,
	BirthDay = @bday,
	Status = @status
	where Id = @personId
END

GO
/****** Object:  StoredProcedure [dbo].[UploadOrUpdatePhoto]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[UploadOrUpdatePhoto]
	@personId uniqueidentifier,
	@content varbinary(max),
	@contentType nvarchar(max)
AS
BEGIN
	if exists (select * from Photos where Id = @personId) 
 update Photos set ContentType = @contentType, Content = @content
 else 
 insert into Photos (Id,Content,ContentType)
 values (@personId,@content,@contentType)
END

GO
/****** Object:  Table [dbo].[Dialogs]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dialogs](
	[ID] [uniqueidentifier] NOT NULL,
	[Participant1_ID] [uniqueidentifier] NULL,
	[Participant2_ID] [uniqueidentifier] NULL,
 CONSTRAINT [PK_dbo.Dialogs] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Messages]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Messages](
	[ID] [uniqueidentifier] NOT NULL,
	[SendDate] [datetime] NOT NULL,
	[IsSeen] [bit] NOT NULL,
	[Content] [nvarchar](max) NULL,
	[From_Id] [uniqueidentifier] NULL,
	[Dialog_ID] [uniqueidentifier] NULL,
 CONSTRAINT [PK_dbo.Messages] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[People]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[People](
	[ID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Surname] [nvarchar](max) NULL,
	[SecondName] [nvarchar](max) NULL,
	[BirthDay] [datetime] NULL,
	[Status] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.People] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PersonFollowers]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersonFollowers](
	[Person_ID] [uniqueidentifier] NOT NULL,
	[Follower_ID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_dbo.PersonPersons] PRIMARY KEY CLUSTERED 
(
	[Person_ID] ASC,
	[Follower_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Photos]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Photos](
	[ID] [uniqueidentifier] NOT NULL,
	[Content] [varbinary](max) NULL,
	[ContentType] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Photos] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[ID] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_dbo.Roles] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserRoles]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRoles](
	[UserID] [uniqueidentifier] NOT NULL,
	[RoleID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_UserRoles] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC,
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 6/29/2016 5:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[ID] [uniqueidentifier] NOT NULL,
	[Login] [nvarchar](200) NOT NULL,
	[RegistrationDate] [datetime] NOT NULL,
	[PasswordHash] [nvarchar](max) NOT NULL,
	[PasswordChangeDate] [datetime] NULL,
 CONSTRAINT [PK_dbo.Users] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Index [IX_Participant1_Id]    Script Date: 6/29/2016 5:21:31 PM ******/
CREATE NONCLUSTERED INDEX [IX_Participant1_Id] ON [dbo].[Dialogs]
(
	[Participant1_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Participant2_Id]    Script Date: 6/29/2016 5:21:31 PM ******/
CREATE NONCLUSTERED INDEX [IX_Participant2_Id] ON [dbo].[Dialogs]
(
	[Participant2_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Dialog_ID]    Script Date: 6/29/2016 5:21:31 PM ******/
CREATE NONCLUSTERED INDEX [IX_Dialog_ID] ON [dbo].[Messages]
(
	[Dialog_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_From_Id]    Script Date: 6/29/2016 5:21:31 PM ******/
CREATE NONCLUSTERED INDEX [IX_From_Id] ON [dbo].[Messages]
(
	[From_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Id]    Script Date: 6/29/2016 5:21:31 PM ******/
CREATE NONCLUSTERED INDEX [IX_Id] ON [dbo].[People]
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Person_Id]    Script Date: 6/29/2016 5:21:31 PM ******/
CREATE NONCLUSTERED INDEX [IX_Person_Id] ON [dbo].[PersonFollowers]
(
	[Person_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Person_Id1]    Script Date: 6/29/2016 5:21:31 PM ******/
CREATE NONCLUSTERED INDEX [IX_Person_Id1] ON [dbo].[PersonFollowers]
(
	[Follower_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Id]    Script Date: 6/29/2016 5:21:31 PM ******/
CREATE NONCLUSTERED INDEX [IX_Id] ON [dbo].[Photos]
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Users]    Script Date: 6/29/2016 5:21:31 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Users] ON [dbo].[Users]
(
	[Login] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Dialogs] ADD  CONSTRAINT [DF_Dialogs_ID]  DEFAULT (newid()) FOR [ID]
GO
ALTER TABLE [dbo].[Messages] ADD  CONSTRAINT [DF_Messages_ID]  DEFAULT (newid()) FOR [ID]
GO
ALTER TABLE [dbo].[Dialogs]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Dialogs_dbo.People_Participant1_Id] FOREIGN KEY([Participant1_ID])
REFERENCES [dbo].[People] ([ID])
GO
ALTER TABLE [dbo].[Dialogs] CHECK CONSTRAINT [FK_dbo.Dialogs_dbo.People_Participant1_Id]
GO
ALTER TABLE [dbo].[Dialogs]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Dialogs_dbo.People_Participant2_Id] FOREIGN KEY([Participant2_ID])
REFERENCES [dbo].[People] ([ID])
GO
ALTER TABLE [dbo].[Dialogs] CHECK CONSTRAINT [FK_dbo.Dialogs_dbo.People_Participant2_Id]
GO
ALTER TABLE [dbo].[Messages]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Messages_dbo.Dialogs_Dialog_ID] FOREIGN KEY([Dialog_ID])
REFERENCES [dbo].[Dialogs] ([ID])
GO
ALTER TABLE [dbo].[Messages] CHECK CONSTRAINT [FK_dbo.Messages_dbo.Dialogs_Dialog_ID]
GO
ALTER TABLE [dbo].[Messages]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Messages_dbo.People_From_Id] FOREIGN KEY([From_Id])
REFERENCES [dbo].[People] ([ID])
GO
ALTER TABLE [dbo].[Messages] CHECK CONSTRAINT [FK_dbo.Messages_dbo.People_From_Id]
GO
ALTER TABLE [dbo].[People]  WITH CHECK ADD  CONSTRAINT [FK_dbo.People_dbo.Users_Id] FOREIGN KEY([ID])
REFERENCES [dbo].[Users] ([ID])
GO
ALTER TABLE [dbo].[People] CHECK CONSTRAINT [FK_dbo.People_dbo.Users_Id]
GO
ALTER TABLE [dbo].[PersonFollowers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.PersonPersons_dbo.People_Person_Id] FOREIGN KEY([Person_ID])
REFERENCES [dbo].[People] ([ID])
GO
ALTER TABLE [dbo].[PersonFollowers] CHECK CONSTRAINT [FK_dbo.PersonPersons_dbo.People_Person_Id]
GO
ALTER TABLE [dbo].[PersonFollowers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.PersonPersons_dbo.People_Person_Id1] FOREIGN KEY([Follower_ID])
REFERENCES [dbo].[People] ([ID])
GO
ALTER TABLE [dbo].[PersonFollowers] CHECK CONSTRAINT [FK_dbo.PersonPersons_dbo.People_Person_Id1]
GO
ALTER TABLE [dbo].[Photos]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Photos_dbo.People_Id] FOREIGN KEY([ID])
REFERENCES [dbo].[People] ([ID])
GO
ALTER TABLE [dbo].[Photos] CHECK CONSTRAINT [FK_dbo.Photos_dbo.People_Id]
GO
ALTER TABLE [dbo].[UserRoles]  WITH CHECK ADD  CONSTRAINT [FK_UserRoles_Roles] FOREIGN KEY([RoleID])
REFERENCES [dbo].[Roles] ([ID])
GO
ALTER TABLE [dbo].[UserRoles] CHECK CONSTRAINT [FK_UserRoles_Roles]
GO
ALTER TABLE [dbo].[UserRoles]  WITH CHECK ADD  CONSTRAINT [FK_UserRoles_Users] FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([ID])
GO
ALTER TABLE [dbo].[UserRoles] CHECK CONSTRAINT [FK_UserRoles_Users]
GO
USE [master]
GO
ALTER DATABASE [FaceFind.Models.FaceFindDbContext] SET  READ_WRITE 
GO
