﻿using FaceFind.Bll;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

namespace FaceFind.Models
{
    public class CustomRoleProvider : RoleProvider
    {
        IApplicationLogic logic;
        public CustomRoleProvider()
        {
            logic = new ApplicationLogic();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            return logic.IsUserInRole(username, roleName);
        }
        public override string[] GetRolesForUser(string username)
        {

            var roles = logic.GetRolesForUser(username);
            var result = new List<string>();
            foreach (var role in roles)
            {
                result.Add(role.Title);
            }
            return result.ToArray();
        }
        #region Unused
        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }
        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        } 
        #endregion
    }
}
