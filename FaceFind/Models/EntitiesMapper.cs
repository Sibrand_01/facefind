﻿using FaceFind.Bll;
using FaceFind.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceFind.Models
{
    /// <summary>
    /// Содержит методы для маппинга доменных моделей в модели представления
    /// </summary>
    public static class EntitiesMapper
    {
        public static User UserFromUserRegisterVm(UserRegisterVM vm)
        {
            return new User()
            {
                Login = vm.Login,
                Person = new Person(vm.Name, vm.Surname),
                PasswordHash = HashGenerator.GetHash(vm.Password),
                PasswordChangeDate = DateTime.Now,
                RegistrationDate = DateTime.Now
            };
        }
    }
}
