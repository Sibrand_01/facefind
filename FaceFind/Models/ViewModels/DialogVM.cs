﻿using FaceFind.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FaceFind.Models
{
    public class DialogVM
    {
        public DialogVM(Dialog dialog, Guid currentPersonId)
        {
            Id = dialog.Id;
            var receiver = dialog.Participant1.Id == currentPersonId ? dialog.Participant2 : dialog.Participant1;
            ReceiverID = receiver.Id;
            ReceiverName = receiver.Name;
            ReceiverSurname = receiver.Surname;
            LastMessage = dialog.Messages.OrderBy(x => x.SendDate).LastOrDefault();
        }

        public Guid Id { get; set; }
        public Guid ReceiverID { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverSurname { get; set; }
        public Message LastMessage { get; set; }
    }
}