﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceFind.Models
{
    /// <summary>
    /// Модель представления пользователя для регистрации
    /// </summary>
    public class UserRegisterVM
    {
        [Required(ErrorMessage = "Обязательное поле")]
        [Display(Name = "Логин")]

        public string Login { get; set; }

        [Required(ErrorMessage = "Обязательное поле")]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]

        public string Password { get; set; }

        [Required(ErrorMessage = "Обязательное поле")]
        [Compare("Password", ErrorMessage = "Поле потверждения не совпадает с основным полем")]
        [DataType(DataType.Password)]
        [Display(Name = "Потверждение пароля")]

        public string ConfirmPassword { get; set; }


        [Required(ErrorMessage = "Обязательное поле")]
        [Display(Name = "Имя")]

        public string Name { get; set; }
        [Required(ErrorMessage = "Обязательное поле")]
        [Display(Name = "Фамилия")]
         
        public string Surname { get; set; }
    }
}
