﻿using FaceFind.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FaceFind.Models
{

    /// <summary>
    /// Класс для редактирования и отображения личной информации о человеке
    /// </summary>
    public class PersonVM
    {
        public PersonVM(Person person)
        {
            Name = person.Name;
            Surname = person.Surname;
            SecondName = person.SecondName;
            BirthDay = person.BirthDay;
            Status = person.Status;
            HasPhoto = person.Photo != null;
            Id = person.Id;
            
        }
        [Required(ErrorMessage = "Обязательное поле")]
        [Display(Name = "Имя")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Обязательное поле")]
        [Display(Name = "Фамилия")]
        public string Surname { get; set; }
        [Display(Name = "Отчество")]

        public string SecondName { get; set; }
        [Display(Name = "Дата рождения")]
        [DisplayFormat(DataFormatString = "{0:d}")]
        public DateTime? BirthDay { get; set; }
        [Display(Name = "Статус")]
        public string Status { get; set; }

        public bool HasPhoto { get; set; }

        [HiddenInput(DisplayValue =false)]
        public Guid Id { get; set; }

    }
}
