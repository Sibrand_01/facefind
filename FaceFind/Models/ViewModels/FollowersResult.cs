﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceFind.Models
{
    public class FollowersResult
    {
        public IList<PersonVM> Followers { get; set; }
        public IList<PersonVM> Followed { get; set; }

        public FollowersResult()
        {
            Followed = new List<PersonVM>();
            Followers = new List<PersonVM>();
        }
    }
}
