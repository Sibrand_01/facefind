﻿using FaceFind.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FaceFind.Models
{
    public class MessageVM
    {
        public MessageVM(Message msg)
        {
            Id = msg.Id;
            Content = msg.Content;
            SendDate = msg.SendDate;
            IsSeen = msg.IsSeen;
            SenderID = msg.From.Id;
            Name = msg.From.Name;
        }

        public Guid Id { get; set; }
        public string Content { get; set; }
        public DateTime SendDate { get; set; }
        public bool IsSeen { get; set; }

        public Guid SenderID { get; set; }
        public string Name { get; set; }
    }
}