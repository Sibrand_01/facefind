﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceFind.Models
{

    /// <summary>
    /// Модель представления для смены пароля
    /// </summary>
    public class PasswordChangeVM
    {
        
        [Required(ErrorMessage ="Обязательное поле")]
        [DataType(DataType.Password)]
        [Display(Name = "Старый пароль")]

        public string Password { get; set; }

        [Required(ErrorMessage = "Обязательное поле")]
        [DataType(DataType.Password)]
        [Display(Name = "Новый пароль")]

        public string NewPassword { get; set; }

        [Required(ErrorMessage = "Обязательное поле")]
        [Compare("NewPassword", ErrorMessage = "Поле потверждения не совпадает с основным полем")]
        [DataType(DataType.Password)]
        [Display(Name = "Подтверждение нового пароля")]

        public string ConfirmNewPassword { get; set; }


        [Display(Name = "Последняя дата смены пароля")]
        public DateTime? PasswordChangeDate { get; set; }


    }
}
