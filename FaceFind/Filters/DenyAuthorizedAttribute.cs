﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace FaceFind.Filters
{
    /// <summary>
    /// Запрещает доступ авторизованным пользователям и отправляет их на домашнюю страницу
    /// </summary>
    public class DenyAuthorizedAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool result = !base.AuthorizeCore(httpContext);
            if(!result)
            {
                httpContext.Response.Redirect("~/Home/Index");
            }
            return result;
        }
    }

}
