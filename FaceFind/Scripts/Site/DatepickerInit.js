﻿$(function () {
    $('.datepicker').datepicker({
        format: "yyyy-mm-dd",
        startDate: "1930-01-01",
        endDate: "0d",
        autoclose: true
    });
});