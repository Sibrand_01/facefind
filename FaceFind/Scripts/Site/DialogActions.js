﻿jQuery(document).ready(function ($) {
   
    $('#sendBtn').on('click', function () {
        var content = $('#sendContent').val();
        var id = $(this).data('dialog');
        $.ajax({
            url: '/Messages/SendMessage',
            type:"POST",
            data: {
                DialogId: id,
                Content: content
            }
        })
    });
});