﻿/// <reference path="~/Scripts/jquery-2.2.3.js" />
/// <reference path="~/Scripts/bootstrap.min.js" />
(function () {
    var nameFilter = $(".filter-name");
    var surnamenameFilter = $(".filter-surname");
    var secondnameFilter = $(".filter-secondname");
    var birthdayMinFilter = $(".filter-birthday-min");
    var birthdayMaxFilter = $(".filter-birthday-max");
    var statusFilter = $(".filter-status");
    function isValidDate(dateString) {
        if (!dateString) {
            return false;
        }

        var regEx = /^\d{4}-\d{2}-\d{2}$/;
        return dateString.match(regEx) != null;
    }

    $(".filter").change(function () {
        var defaultValue = "Не указано";

        var name = nameFilter.val();
        var surname = surnamenameFilter.val();
        var secondname = secondnameFilter.val();

        var birthdayMin = "";
        if (birthdayMinFilter.val() && isValidDate(birthdayMinFilter.val())) {
            birthdayMin = Date.parse(birthdayMinFilter.val());
        }
        var birthdayMax = "";

        if (birthdayMaxFilter.val() && isValidDate(birthdayMaxFilter.val())) {
            birthdayMax = Date.parse(birthdayMaxFilter.val());
        }
        var status = statusFilter.val();

        $(".person").each(function (index, element) {
            var match = true; //по этой переменной после проверок по всем фильтрам можно будет определить, что элемент нужно отобразить

            //Проверяем данные в списке на соответсвие каждому параметру
            if (name) {

                var personName = $(this).children(".person-name").text().toString();
                if (personName !== defaultValue) {
                    if (personName.indexOf(name) < 0) {
                        match = false;
                        $(this).hide(); //Если какой-либо параметр не подошел, данный человек не будет отображаться в списке
                    }
                }
                
            }
            if (surname && match) { // если match уже false, то проверки можно пропускать

                var personSurname = $(this).children(".person-surname").text().toString();

                if (personSurname !== defaultValue) {

                    if (personSurname.indexOf(surname) < 0) {
                        match = false;
                        $(this).hide();
                    }
                }
            }

            if (secondname && match) {
                var personSecondname = $(this).children(".person-secondname").text().toString();

                if (personSecondname !== defaultValue) {

                    if (personSecondname.indexOf(secondname) < 0) {
                        match = false;
                        $(this).hide();
                    }
                }
            }

            if (birthdayMin && match) {
                var personBirthdayMin = Date.parse($(this).children(".person-birthDay").text().toString());
                if (personBirthdayMin < birthdayMin) {
                    match = false;
                    $(this).hide();
                }
            }

            if (birthdayMax && match) {
                var personBirthdayMax = Date.parse($(this).children(".person-birthDay").text().toString());
                if (personBirthdayMax > birthdayMax) {
                    match = false;
                    $(this).hide();
                }
            }

            if (status  && match) {
                var personStatus = $(this).children(".person-status").text().toString();
                if (personStatus !== defaultValue) {

                    if (personStatus.indexOf(status) < 0) {
                        match = false;
                        $(this).hide();
                    }
                }
            }

            if (match) { // отображаем человека, если все проверки пройдены
                $(this).show();
            }
        });

    })

})()