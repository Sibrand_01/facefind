﻿/// <reference path="~/Scripts/jquery-2.2.3.js" />
/// <reference path="~/Scripts/bootstrap.min.js" />
(function () {

    $('.person').on('click', '.remove-admin-action', function (e) {
        var $elem = $(e.currentTarget);
        var personId = $($elem.parents('.person')[0]).attr('id');
        $.ajax({
            url: ''.concat('/Home/RemoveAdmin?personId=', personId),
            method: 'GET',

        }).success(function (data) {
            $elem.toggleClass('remove-admin-action');
            $elem.toggleClass('set-admin-action');
            $elem.children().toggleClass('glyphicon-hand-down');
            $elem.children().toggleClass('glyphicon-hand-up');
        });

    });

    $('.person').on('click', '.set-admin-action', function (e) {
        var $elem = $(e.currentTarget);
        var personId = $($elem.parents('.person')[0]).attr('id');
        $.ajax({
            url: ''.concat('/Home/SetAdmin?personId=', personId),
            method: 'GET',

        }).success(function (data) {
            $elem.toggleClass('remove-admin-action');
            $elem.toggleClass('set-admin-action');
            $elem.children().toggleClass('glyphicon-hand-down');
            $elem.children().toggleClass('glyphicon-hand-up');
        });

    });

    $('.person').on('click', '.ban-action', function (e) {
        var $elem = $(e.currentTarget);
        var personId = $($elem.parents('.person')[0]).attr('id');
        $.ajax({
            url: ''.concat('/Home/BanPerson?personId=', personId),
            method: 'GET',

        }).success(function (data) {
            $elem.toggleClass('ban-action');
            $elem.toggleClass('unban-action');
            $elem.text('Разбанить');
        });

    });

    $('.person').on('click', '.unban-action', function (e) {
        var $elem = $(e.currentTarget);
        var personId = $($elem.parents('.person')[0]).attr('id');
        $.ajax({
            url: ''.concat('/Home/UnbanPerson?personId=', personId),
            method: 'GET',

        }).success(function (data) {
            $elem.toggleClass('ban-action');
            $elem.toggleClass('unban-action');
            $elem.text('Забанить');
        });

    });

    $('.person').on('click', '.follow-action', function (e) {
        var $elem = $(e.currentTarget);
        var personId = $($elem.parents('.person')[0]).attr('id');
        $.ajax({
            url: ''.concat('/Home/FollowPerson?personId=', personId),
            method: 'GET',
            
        }).success(function (data) {
            $elem.toggleClass('follow-action');
            $elem.toggleClass('unfollow-action');
            $elem.children().toggleClass('glyphicon-plus');
            $elem.children().toggleClass('glyphicon-minus');
        });
    });

$('.person').on('click', '.unfollow-action', function (e) {
    var $elem = $(e.currentTarget);
    var personId = $($elem.parents('.person')[0]).attr('id');
    $.ajax({
        url: ''.concat('/Home/UnfollowPerson?personId=', personId),
        method: 'GET',

    }).success(function (data) {
        $elem.toggleClass('follow-action');
        $elem.toggleClass('unfollow-action');
        $elem.children().toggleClass('glyphicon-plus');
        $elem.children().toggleClass('glyphicon-minus');
    });

});

})();