﻿using FaceFind.Bll;
using FaceFind.Entities;
using FaceFind.Filters;
using FaceFind.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FaceFind.Controllers
{
    /// <summary>
    /// Содержит основные операции с фотографиями
    /// </summary>
    public class ImageController : Controller
    {
        private IApplicationLogic logic;

        /// <summary>
        /// Метод проверяет, является ли загруженный файл изображением
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private bool IsImage(HttpPostedFileBase file)
        {
            if(file == null)
            {
                return false;
            }
            if (file.ContentType.Contains("image"))
            {
                return true;
            }

            string[] formats = new string[] { ".jpg", ".png", ".gif", ".jpeg" };

            return formats.Any(item => file.FileName.EndsWith(item, StringComparison.OrdinalIgnoreCase));
        }
        public ImageController()
        {
            logic = new ApplicationLogic();
        }
        /// <summary>
        /// Возвращает страницу загрузки фотографии
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles =Role.SuperAdminRoleTitle + ", " + Role.AdminRoleTitle + ", " + Role.UserRoleTitle)]
        public ActionResult UploadImage()
        {
            return View();
        }
        /// <summary>
        /// Загружает полученную фотографию в профиль текущего пользователя
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        [Authorize(Roles =Role.SuperAdminRoleTitle + ", " + Role.AdminRoleTitle + ", " + Role.UserRoleTitle)]
        [HttpPost]
        public ActionResult UploadImage(HttpPostedFileBase image)
        {
            if (IsImage(image))
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    image.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                    Photo photo = new Photo();
                    photo.Content = array;
                    photo.ContentType = image.ContentType;
                    logic.AddOrChangePhoto(photo, User.Identity.Name);
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                ViewBag.Error = "Загруженный файл не является изображением";
                return View();
            }

        }
        /// <summary>
        /// Отображает фотографию пользователя по id его личных данных
        /// </summary>
        /// <param name="personId"></param>
        /// <returns></returns>
        [Authorize(Roles =Role.SuperAdminRoleTitle + ", " + Role.AdminRoleTitle + ", " + Role.UserRoleTitle)]
        public ActionResult DisplayUserImage(Guid personId)
        {
            var person = logic.GetPersonByPersonId(personId);
            if (person.Photo == null)
            {
                return File("~/Content/Images/placeholder.jpg", "image/jpeg");
            }
            return File(person.Photo.Content, person.Photo.ContentType);
        }
    }
}