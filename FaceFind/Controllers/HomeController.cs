﻿using FaceFind.Bll;
using FaceFind.Entities;
using FaceFind.Filters;
using FaceFind.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace FaceFind.Controllers
{
    /// <summary>
    /// Содержит основные пользовательские операции
    /// </summary>
    public class HomeController : Controller
    {
        
        private IApplicationLogic logic;


        public HomeController()
        {
            logic = new ApplicationLogic();
        }

        /// <summary>
        /// Возврщает домашнюю страницу с основной информацией о текущем пользователе.
        /// <returns></returns>
        [Authorize]
        // GET: Home
        public ActionResult Index()
        {
            var currentUser = logic.GetUserByLogin(User.Identity.Name);
            Person person = currentUser.Person;
            var personVM = new PersonVM(person);
            return View(personVM);
        }

        /// <summary>
        /// Возращает страницу редактирования личных данных
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public ActionResult PersonEdit()
        {
            var currentPerson = logic.GetPersonByUserLogin(User.Identity.Name);
            var personVM = new PersonVM(currentPerson);
            return View(personVM);
        }
        /// <summary>
        /// Редактирует личную информацию пользователя, используя полученные данные
        /// </summary>
        /// <param name="person"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ActionResult PersonEdit(Person person)
        {
            if (ModelState.IsValid)
            {
                logic.EditPersonInfo(person, User.Identity.Name);
                return RedirectToAction("Index");
            }
            else
            {
                return View(person);
            }
        }


        /// <summary>
        /// Возвращает информацию о всех зарегистрированных пользователях.
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public ActionResult GetAllPeople()
        {
            var people = logic.GetAllPeople().ToList();
            List<PersonVM> peopleVM = new List<PersonVM>();
            foreach (var person in people)
            {
                peopleVM.Add(new PersonVM(person));
            }
            var current = logic.GetPersonByUserLogin(User.Identity.Name);
            ViewBag.CurrentPersonId = current.Id;
            ViewBag.UserFollowers = logic.GetUserFollowers(User.Identity.Name).Select(x => x.Id);
            ViewBag.BannedPersons = logic.GetUsersIdByRole(Role.BannedRoleTitle);
            ViewBag.Admins = logic.GetUsersIdByRole(Role.AdminRoleTitle);
            return View(peopleVM);
        }

        /// <summary>
        /// Возвращает личную информаццию о человеке
        /// </summary>
        /// <param name="personId"></param>
        /// <returns></returns>
        [Authorize]
        public ActionResult GetPerson(Guid personId)
        {
            var person = logic.GetPersonByPersonId(personId);
            var currentUser = logic.GetUserByLogin(User.Identity.Name);
            var personVM = new PersonVM(person);

            if (currentUser.Person == person)
            {
                return View("Index", "", personVM);
            }
            else
            {
                return View(personVM);
            }
        }

        /// <summary>
        /// Возвращает страницу со списком подпищиков
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public ActionResult Followers()
        {
            var res = new FollowersResult();
            var followers = logic.GetUserFollowers(User.Identity.Name);
            foreach (var item in followers)
            {
                res.Followers.Add(new PersonVM(item));
            }
            var followed = logic.GetFollowedPersons(User.Identity.Name);
            foreach (var item in followed)
            {
                res.Followed.Add(new PersonVM(item));
            }
            ViewBag.UserFollowers = logic.GetUserFollowers(User.Identity.Name).Select(x => x.Id);
            ViewBag.BannedPersons = logic.GetUsersIdByRole(Role.BannedRoleTitle);
            ViewBag.Admins = logic.GetUsersIdByRole(Role.AdminRoleTitle);
            return View(res);
        }


        /// <summary>
        /// Подписывается на человека
        /// </summary>
        /// <param name="personId"></param>
        /// <returns></returns>
        [Authorize]
        public ActionResult FollowPerson(Guid personId)
        {
            logic.FollowPerson(personId, User.Identity.Name);
            return this.Json(string.Empty, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Отписывается от человека
        /// </summary>
        /// <param name="personId"></param>
        /// <returns></returns>
        [Authorize]
        public ActionResult UnfollowPerson(Guid personId)
        {
            logic.UnfollowPerson(personId, User.Identity.Name);
            return this.Json(string.Empty, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Банит пользователя
        /// </summary>
        /// <param name="personId"></param>
        /// <returns></returns>
        [Authorize(Roles = Role.SuperAdminRoleTitle + ", " + Role.AdminRoleTitle)]
        public ActionResult BanPerson(Guid personId)
        {
            logic.BanPerson(personId);
            return this.Json(string.Empty, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Разбанивает пользователя
        /// </summary>
        /// <param name="personId"></param>
        /// <returns></returns>
        [Authorize(Roles = Role.SuperAdminRoleTitle + ", " + Role.AdminRoleTitle)]
        public ActionResult UnbanPerson(Guid personId)
        {
            logic.UnbanPerson(personId);
            return this.Json(string.Empty, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = Role.SuperAdminRoleTitle + ", " + Role.AdminRoleTitle)]
        public ActionResult SetAdmin(Guid personId)
        {
            var user = logic.GetPersonByPersonId(personId).User;
            logic.AddUserRole(user.Login,Role.AdminRoleTitle);
            return this.Json(string.Empty, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = Role.SuperAdminRoleTitle + ", " + Role.AdminRoleTitle)]
        public ActionResult RemoveAdmin(Guid personId)
        {
            var user = logic.GetPersonByPersonId(personId).User;
            logic.DeleteUserRole(user.Login, Role.AdminRoleTitle);
            return this.Json(string.Empty, JsonRequestBehavior.AllowGet);
        }
    }
}

   