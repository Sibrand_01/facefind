﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FaceFind.Models;
using System.Web.Security;
using FaceFind.Filters;
using FaceFind.Bll;
using FaceFind.Entities;

namespace FaceFind.Controllers
{

    public class AccountController : Controller
    {
        private IApplicationLogic logic;

        public AccountController()
        {
            logic = new ApplicationLogic();
        }
        // GET: Authentication
        [DenyAuthorized]
        public ActionResult Login()
        {

            return View();
        }

        [DenyAuthorized]
        [HttpPost]
        public ActionResult Login(UserLoginVM model)
        {
            if (ModelState.IsValid)
            {
                if (logic.CanAuthenticate(model.Login, model.Password))
                {
                    FormsAuthentication.SetAuthCookie(model.Login, true);

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Логин и/или пароль не верны");
                    return View(model);
                }
            }
            return View(model);
        }

        [DenyAuthorized]
        public ActionResult Register()
        {
            return View();
        }
        [DenyAuthorized]
        [HttpPost]
        public ActionResult Register(UserRegisterVM model)
        {
            if (ModelState.IsValid)
            {
                if (logic.IsNewLogin(model.Login))
                {

                    User newUser = EntitiesMapper.UserFromUserRegisterVm(model);
                    logic.AddNewUser(newUser);
                    FormsAuthentication.SetAuthCookie(model.Login, true);

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Данный логин уже существует");
                    return View(model);
                }
            }
            return View(model);

        }


        [Authorize]
        public ActionResult ChangePassword()
        {
            var lastChangeDate = logic.GetUserByLogin(User.Identity.Name).PasswordChangeDate;
            return View(new PasswordChangeVM() { PasswordChangeDate = lastChangeDate });
        }

        [HttpPost]
        [Authorize]
        public ActionResult ChangePassword(PasswordChangeVM model)
        {
            if (ModelState.IsValid)
            {
                if (logic.CanAuthenticate(User.Identity.Name, model.Password))
                {
                    logic.ChangePassword(User.Identity.Name, model.NewPassword);

                    return RedirectToAction("Index", "Home");

                }
                else
                {
                    ModelState.AddModelError("", "Старый пароль не верен");
                }
            }
            model.PasswordChangeDate = logic.GetUserByLogin(User.Identity.Name).PasswordChangeDate;
            return View(model);
        }

        [Authorize]
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }
    }
}