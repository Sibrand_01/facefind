﻿using FaceFind.Bll;
using FaceFind.Entities;
using FaceFind.Filters;
using FaceFind.Models;
using FaceFindBll.Interfaces;
using FaceFindBll.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FaceFind.Controllers
{
    [Authorize, DenyAuthorized(Roles = Role.BannedRoleTitle)]
    public class MessagesController : Controller
    {
        private IMessagesLogic logic;


        public MessagesController()
        {
            logic = new MessagesLogic();
        }

        public ActionResult Dialogs()
        {
            var dialogs = logic.GetUserDialogs(User.Identity.Name);
            var currentUser = logic.GetPersonByUserLogin(User.Identity.Name);
            var res = new List<DialogVM>();
            foreach (var item in dialogs)
            {
                res.Add(new DialogVM(item, currentUser.Id));
            }
            return View(res);
        }

        public ActionResult Dialog(Guid DialogId)
        {
            var dialog = logic.GetUserDialog(User.Identity.Name, DialogId);
            var res = new List<MessageVM>();
            foreach (var item in dialog)
            {
                res.Add(new MessageVM(item));
            }
            ViewBag.DialogId = DialogId;
            return View(res);
        }

        public ActionResult OpenDialog(Guid personId)
        {

            Guid dialogId = logic.GetDialogId(personId, User.Identity.Name);
            return RedirectToAction("Dialog", new { DialogId = dialogId });
        }

        [HttpPost]
        public ActionResult SendMessage(Guid DialogId, string Content)
        {
            var msg = new Message(Content);
            var current = logic.GetPersonByUserLogin(User.Identity.Name);
            msg.From = current;
            logic.SendMessage(msg, DialogId);

            var msgVM = new MessageVM(msg);
            return PartialView(msgVM);
        }
    }
}