﻿using System;
using System.Threading;

namespace Common
{
    public sealed class WriteLockHolder : LockHolder
    {
        public WriteLockHolder(ReaderWriterLockSlim locker)
            : base(locker, x => x.EnterWriteLock(), x => x.ExitWriteLock())
        {
        }
    }
}
