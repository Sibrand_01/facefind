﻿using System;
using System.Threading;

namespace Common
{
    public sealed class ReadLockHolder : LockHolder
    {
        public ReadLockHolder(ReaderWriterLockSlim locker)
            : base(locker, x => x.EnterReadLock(), x => x.ExitReadLock())
        {
        }
    }
}
