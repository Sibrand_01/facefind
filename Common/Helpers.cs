﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public static class Helpers
    {

        public static V Get<K, V>(this IDictionary<K, V> dict, K key, Func<V> defaultValue = null, bool pushNotFoundToDictionary = true)
        {
            if (!dict.ContainsKey(key))
            {
                defaultValue = defaultValue ?? (Func<V>)(() => default(V));
                var res = defaultValue();
                if (pushNotFoundToDictionary)
                {
                    dict[key] = res;
                }
                return res;
            }
            return dict[key];
        }

        public static IEnumerable<T> ForEach<T>(this IEnumerable<T> items, Action<T> action)
        {
            foreach (var i in items)
            {
                action(i);
            }
            return items;
        }

        public static IEnumerable<T> ForEachSafe<T>(this IEnumerable<T> items, Action<T> action)
        {
            foreach (var i in items.ToArray())
            {
                action(i);
            }
            return items;
        }

        public static string FormatArgs(this string s, params object[] args)
        {
            return string.Format(s, args);
        }

        public static string ToNullSafeString(this object value)
        {
            return value == null ? string.Empty : value.ToString();
        }

        public static StringBuilder AppendWhitespaceAfterLastWord(this StringBuilder sb)
        {
            if (sb[sb.Length-1] != ' ')
                sb.Append(' ');
            return sb;
        }
    }
}
