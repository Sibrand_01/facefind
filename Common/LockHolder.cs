﻿using System;
using System.Diagnostics;
using System.Threading;

namespace Common
{
    public abstract class LockHolder : IDisposable
    {
        private readonly ReaderWriterLockSlim _locker;
        private readonly Action<ReaderWriterLockSlim> _unlockAction;

        protected LockHolder(ReaderWriterLockSlim locker, Action<ReaderWriterLockSlim> lockAction, Action<ReaderWriterLockSlim> unlockAction)
        {
            ArgumentGuard.ThrowOnNull(locker, "locker");
            ArgumentGuard.ThrowOnNull(lockAction, "lockAction");
            ArgumentGuard.ThrowOnNull(unlockAction, "unlockAction");

            _locker = locker;
            _unlockAction = unlockAction;

            lockAction(_locker);
        }

        ~LockHolder()
        {
            Debug.WriteLine("[TO FIX]: LockHolder object has been Finalized. Could lead to extra locking or even a deadlock.");
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            _unlockAction(_locker);
        }
    }
}
